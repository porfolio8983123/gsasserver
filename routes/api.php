<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AcademyController;
use App\Http\Controllers\DzongkhagController;
use App\Http\Controllers\SkillingController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\EnlisteeController;
use App\Http\Controllers\AcademySkillingController;
use App\Http\Controllers\AllocationController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthenticationController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[AuthenticationController::class,'login']);
Route::post('register',[AuthenticationController::class,'register']);

Route::middleware('auth:api')->group(function () {
    Route::post('logout',[AuthenticationController::class,'logout']);
    Route::resource('academy',AcademyController::class);
    Route::get('dzongkhag',[DzongkhagController::class,'index']);
    Route::patch('academy/{academy}/updateStatus',[AcademyController::class,'updateStatus']);
    Route::get('skilling',[SkillingController::class,'getSkilling']);
    Route::put('skilling/{academySkillingId}',[SkillingController::class,'editAcademySkilling']);
    Route::post('skilling',[SkillingController::class,'addNewService']);
    Route::delete('skilling/delete/{academySkillingId}',[SkillingController::class,'deleteService']);
    Route::get("/getSkilling",[SettingController::class,'getAcademySkilling']);
    Route::post('checkFileName',[FileUploadController::class,'checkFileName']);
    Route::post('fileUpload',[FileUploadController::class,'upload']);
    Route::delete('deleteFile/{fileId}',[FileUploadController::class,'deleteFile']);
    Route::get('getAllCSVFile',[FileUploadController::class,'index']);
    Route::resource('enlistee',EnlisteeController::class);
    Route::get('getAcademySkilling/{id}',[AcademySkillingController::class,'getAcademySkilling']);
    Route::get('getAcademy/{id}',[AcademySkillingController::class,'getAcademy']);
    Route::post('startAllocation',[AllocationController::class,'startAllocation']);
    Route::post('ictAllocation',[AllocationController::class,'ictAllocation']);
    Route::post('cssAllocation',[AllocationController::class,'cssAllocation']);
    Route::get('getAllAllocation',[AllocationController::class,'getAllAllocation']);
    Route::post('hssAllocation',[AllocationController::class,'hssAllocation']);
    Route::post('fssAllocation',[AllocationController::class,'fssAllocation']);
    Route::post('deleteAllocation',[AllocationController::class,'deleteAllocation']);
    Route::get('getAllocation/{allocationId}',[AllocationController::class,'getAllocation']);
    Route::get('getAcademyData/{allocationId}',[AllocationController::class,'getAcademyData']);
    Route::get('getAllCurrentAllocation/{allocationId}',[AllocationController::class,'getAllCurrentAllocation']);
    Route::put('saveAllocation/{allocationId}',[AllocationController::class,'saveAllocation']);
    Route::get('getSelectedAllocation',[AllocationController::class,'getSelectedAllocation']);
    Route::get('getAdmin',[AdminController::class,'index']);
    Route::post('changeAdminPassword',[AdminController::class,'changePassword']);
});