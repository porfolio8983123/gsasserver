<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('academy_skilling', function (Blueprint $table) {
            $table->id('academySkillingId');
            $table->unsignedBigInteger('academyId');
            $table->foreign('academyId')->references('academyId')->on('academy');
            $table->unsignedBigInteger('skillingId');
            $table->foreign('skillingId')->references('skillingId')->on('skilling');
            $table->integer('intakeSize');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('academy_skilling');
    }
};
