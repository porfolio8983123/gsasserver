<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gewog', function (Blueprint $table) {
            $table->id('gewogId');
            $table->unsignedBigInteger('dzongkhagId');
            $table->foreign('dzongkhagId')
                ->references('dzongkhagId')
                ->on('dzongkhag')
                ->onDelete('cascade');
            $table->string('gewog');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gewog');
    }
};
