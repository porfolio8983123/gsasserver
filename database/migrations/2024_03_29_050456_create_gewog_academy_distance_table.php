<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gewog_academy_distance', function (Blueprint $table) {
            $table->id('gewogAcademyDistaceId');
            $table->unsignedBigInteger('academyId');
            $table->foreign('academyId')->references('academyId')->on('academy');
            $table->unsignedBigInteger('gewogId');
            $table->foreign('gewogId')->references('gewogId')->on('gewog');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gewog_academy_distance');
    }
};
