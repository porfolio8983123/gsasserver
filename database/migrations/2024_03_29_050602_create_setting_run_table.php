<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('setting_run', function (Blueprint $table) {
            $table->id('settingRunId');
            $table->unsignedBigInteger('allocationId');
            $table->foreign('allocationId')->references('allocationId')->on('allocation');
            $table->unsignedBigInteger('skillingId');
            $table->foreign('skillingId')->references('skillingId')->on('skilling');
            $table->unsignedBigInteger('settingId');
            $table->foreign('settingId')->references('settingId')->on('setting');
            $table->unsignedBigInteger('academySkillingId');
            $table->foreign('academySkillingId')->references('academySkillingId')->on('academy_skilling');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('setting_run');
    }
};
