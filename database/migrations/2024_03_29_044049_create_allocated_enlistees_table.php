<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('allocated_enlistees', function (Blueprint $table) {
            $table->id('CID');
            $table->string('name');
            $table->string('gender');
            $table->unsignedBigInteger('gewogId');
            $table->foreign('gewogId')
                ->references('gewogId')
                ->on('gewog')
                ->onDelete('cascade');
            $table->boolean('hasHigerEducation');
            $table->integer('math')->nullable();
            $table->integer('bmath')->nullable();
            $table->integer('english');
            $table->integer('dzongkha');
            $table->float('weight');
            $table->unsignedBigInteger('intakeId');
            $table->foreign('intakeId')->references('intakeId')->on('intake')->onDelete('cascade');
            $table->unsignedBigInteger('allocationId');
            $table->primary(['CID', 'allocationId']);
            $table->unsignedBigInteger('academySkillingId');
            $table->foreign('academySkillingId')->references('academySkillingId')->on('academy_skilling')->onDelete('cascade');
            $table->foreign('allocationId')->references('allocationId')->on('allocation')->onDelete('cascade');
            $table->boolean('isDiversity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('allocated_enlistees');
    }
};
