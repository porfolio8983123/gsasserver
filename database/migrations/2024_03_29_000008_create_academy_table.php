<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('academy', function (Blueprint $table) {
            $table->id('academyId');
            $table->string('name');
            $table->integer('capacity');
            $table->unsignedBigInteger('dzongkhagId');
            $table->foreign('dzongkhagId')->references('dzongkhagId')->on('dzongkhag');
            $table->boolean('status');
            $table->string('imagePath');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('academy');
    }
};
