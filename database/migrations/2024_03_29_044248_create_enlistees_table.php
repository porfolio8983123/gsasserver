<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('enlistees', function (Blueprint $table) {
            $table->id('CID');
            $table->string('name');
            $table->string('gender');
            $table->unsignedBigInteger('gewogId');
            $table->foreign('gewogId')
                ->references('gewogId')
                ->on('gewog')
                ->onDelete('cascade');
            $table->boolean('hasHigerEducation');
            $table->integer('math')->nullable();
            $table->integer('bmath')->nullable();
            $table->integer('english');
            $table->integer('dzongkha');
            $table->float('weight');
            $table->unsignedBigInteger('preferenceOne')->nullable();
            $table->foreign('preferenceOne')
                ->references('skillingId')
                ->on('skilling')
                ->onDelete('cascade');

            $table->unsignedBigInteger("preferenceTwo")->nullable();
            $table->foreign('preferenceTwo')->references('skillingId')->on('skilling')->onDelete('cascade');
            $table->boolean('noPreference');
            $table->boolean('hasTenFinger');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('enlistees');
    }
};
