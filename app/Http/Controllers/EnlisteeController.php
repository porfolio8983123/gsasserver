<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enlistees;
use App\Models\Gewog;
use App\Models\Skilling;
use Illuminate\Support\Facades\Log;

use DB;

class EnlisteeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $req)
    {
        $enlistees = Enlistee::all();
        return response($enlistees,200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $studentData = $request->input('studentData');

        DB::beginTransaction();

        try {

            Enlistees::query()->delete();

            foreach($studentData[0] as $student) {
                $CID = $student['CID'];
                $name = $student['NAME'];
                $gewog = $student['GEWOG'];
                $gender = $student['SEX'];
                $math = ($student['MATH'] === 'null') ? null : intval($student['MATH']);
                $bmath = ($student['BMATH'] === 'null') ? null : intval($student['BMATH']);
                $english = $student['ENGLISH'];
                $dzongkha = $student['DZONGKHA'];
                $preferenceOne = $student['PREFERENCE1'];
                $preferenceTwo = $student['PREFERENCE2'];
                $noPreference = ($student['NOPREFERENCE'] === 'true');
                $hasTenFinger = ($student['HASTENFINGER'] === 'true');

                $hasHigherEducation = ($student['HASHIGHEREDUCATION'] === 'true');

                if (strlen($gewog) === 0) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Gewog name missing for CID '.$CID
                    ],500);
                }

                try {
                    $gewogId = Gewog::where('gewog',$gewog)->firstOrFail()->gewogId;
                } catch (\Exception $e) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Use the standard gewog name for CID '.$CID
                    ],500);
                }

                try {
                    if (!$noPreference) {
                        $preferenceOneId = Skilling::where('skillingName',$preferenceOne)->firstOrFail()->skillingId;
                        $preferenceTwoId = Skilling::where('skillingName',$preferenceTwo)->firstOrFail()->skillingId;

                        error_log("skilling id of preference 1 ".$preferenceOneId);
                        error_log("skilling id of preference 2 ".$preferenceTwoId);
                    }
                } catch (\Exception $e) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Skilling not found!'.$e->getMessage()
                    ],500);
                }

                Enlistees::create([
                    'CID' => $CID,
                    'name' => $name,
                    'gender' => $gender,
                    'gewogId' => $gewogId,
                    'hasHigerEducation' => $hasHigherEducation,
                    'math' => $math,
                    'bmath' => $bmath,
                    'english' => $english,
                    'dzongkha' => $dzongkha,
                    'weight' => 50,
                    'preferenceOne' => ($noPreference) ? null : $preferenceOneId,
                    'preferenceTwo' => ($noPreference) ? null : $preferenceTwoId,
                    'noPreference' => $noPreference,
                    'hasTenFinger' => $hasTenFinger
                ]);
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message'=>'Data stored successfully!',
                'student data' => $studentData
            ],200);
        }catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $enlistee = Enlistee::findOrFail($id);
        return response($enlistee,200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $req, string $id)
    {
        $enslistee = Enlistee::findorFail($id);
        $enlistee->update([
            'CID'=>$req->CID,
            'name'=>$req->name,
            'gender'=>$req->gender,
            'gewogId'=>$req->gewogId
        ]);

        return response([
            'message'=>'Updated!'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $enlistee = Enlistee::where('CID',$id)->delete();
        return response([
            'message'=>'Deleted!'
        ],200);
    }
}
