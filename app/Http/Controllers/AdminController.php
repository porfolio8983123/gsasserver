<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index() {

        $users = Auth::user();

        return response()->json([
            "users"=>$users
        ],200);
    }

    public function changePassword(Request $request) {

        try {
            // $request->validate([
            //     'current_password' => 'required',
            //     'password' => 'required|string|min:8',
            //     'password_confirmation' => 'required|string|min:8|same:password',
            // ]);
    
            // $user = Auth::user();
    
            // if (!Hash::check($request->input('current_password'), $user->password)) {
            //     return response()->json(['message' => 'Current password is incorrect'], 422);
            // }
    
            // $user->password = Hash::make($request->input('password'));
            // $user->save();
    
            return response()->json(['message' => 'Password changed successfully'], 200);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
