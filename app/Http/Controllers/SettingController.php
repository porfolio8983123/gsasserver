<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Skilling;
use DB;

class SettingController extends Controller
{
    public function getAcademySkilling() {

        $academyskillings = DB::table('academy_skilling')
            ->join('academy','academy_skilling.academyId','=','academy.academyId')
            ->join('skilling','academy_skilling.skillingId','=','skilling.skillingId')
            ->orderByRaw("FIELD(skilling.skillingName, 'ICT', 'CSS', 'HSS', 'FSS')")
            ->get();
        
        return response()->json([
            "data"=>$academyskillings
        ]);
    }
}
