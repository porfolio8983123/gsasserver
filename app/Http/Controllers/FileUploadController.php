<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentFile;
use Illuminate\Support\Facades\Validator;
use File;

class FileUploadController extends Controller
{
    public function index() {
        try {
            $fileEntries = StudentFile::orderBy('created_at', 'desc')->get();

            $fileDetails = [];

            foreach($fileEntries as $fileEntry) {
                $filename = $fileEntry->fileName;

                $filePath = public_path('studentFile\\'.$filename);

                if (file_exists($filePath)) {
                    $fileContent = file_get_contents($filePath);

                    $createdAt = $fileEntry->created_at;

                    $fileDetails[] = [
                        'id'=>$fileEntry->id,
                        'filename'=>$filename,
                        'createdAt'=>$createdAt,
                        'fileContent'=>$fileContent
                    ];
                }
            }

            return response()->json([
                'status' => 'success',
                'message' => 'File details retrieved successfully',
                'fileDetails' => $fileDetails,
            ],200);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage(),
            ],500);
        }
    }

    public function checkFileName(Request $request) {
        try {
            $validator = Validator::make($request->all(),[
                'fileName'=>'required'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status"=>'failed',
                    'message'=>$validator->errors()
                ],500);
            }

            if ($request->has('fileName')) {
                $filename = $request->input('fileName');

                error_log("filename".$filename);

                if (StudentFile::where('fileName', $filename)->exists()) {
                    return response()->json([
                        "status" => 'failed',
                        'message' => 'File with this name already exists!'
                    ], 200);
                } else {
                    return response()->json([
                        "status"=>'success',
                    ],200);
                }
            }
        } catch(\Exception $e) {
            return response()->json([
                'message'=>$e
            ],500);
        }
    }

    public function upload(Request $request) {
        try {
            $validator = Validator::make($request->all(),[
                'fileName'=>'required',
                'file'=>'required'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status"=>'failed',
                    'message'=>$validator->errors()
                ],500);
            }

            if ($request->has('file') && $request->has('fileName')) {
                $filename = $request->input('fileName');
                $file = $request->file('file');

                $file->move('studentFile/',$filename);

                $fileEntry = new StudentFile();
                $fileEntry->fileName = $filename;
                $fileEntry->save();

                return response()->json([
                    "status"=>'success',
                    'message'=>'file uploaded'
                ],200);
            } else {
                return response()->json([
                    "status"=>'error',
                    'message'=>'file not uploaded'
                ],500);
            }
        } catch(\Exception $e) {
            return response()->json([
                'message'=>$e
            ],500);
        }
    }

    public function deleteFile(Request $req, int $id)
    {
        try {
            $fileEntry = StudentFile::find($id);

            if (!$fileEntry) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'File not found',
                ], 404);
            }

            $filename = $fileEntry->fileName;
            $filePath = public_path('studentFile\\' . $filename);

            if (!file_exists($filePath)) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'File not found on server',
                ], 404);
            }

            $deleted = File::delete($filePath);

            if (!$deleted) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Failed to delete file',
                ], 500);
            }

            $fileEntry->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'File deleted successfully',
                'file' => $fileEntry,
                'filename' => $filename,
                'filepath' => $filePath
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
