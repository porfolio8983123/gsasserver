<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Intake;
use App\Models\Allocation;
use App\Models\AcademySkilling;
use App\Models\Setting;
use App\Models\SettingRun;
use App\Models\AllocatedEnlistees;
use Illuminate\Support\Facades\Auth;

class AllocationController extends Controller
{

    public function getSelectedAllocation()
    {
        try {

            $allSelectedAllocation = [];

            $selectedAllocation = Allocation::where('selectedAllocation', true)
            ->orderBy('updated_at', 'desc')
            ->get();

            foreach ($selectedAllocation as $allocation) {

                $allocationId = $allocation->allocationId;

                $academySetting = DB::table('academy_skilling')
                ->join('skilling', 'skilling.skillingId', '=', 'academy_skilling.skillingId')
                ->join('setting_run', 'setting_run.academySkillingId', '=', 'academy_skilling.academySkillingId')
                ->join('setting', 'setting.settingId', '=', 'setting_run.settingId')
                ->join('allocation', 'allocation.allocationId', '=', 'setting_run.allocationId')
                ->where('allocation.allocationId', $allocationId)
                ->select('academy_skilling.*', 'skilling.*', 'setting_run.*', 'setting.*', 'allocation.*')
                ->distinct()
                ->get();
    
                $academyStudents = [];
        
                foreach ($academySetting as $setting) {
                    // Filter students for the specific academy
                    $eachStudent = DB::table('allocated_enlistees')
                        ->join('allocation', 'allocated_enlistees.allocationId', '=', 'allocation.allocationId')
                        ->join('academy_skilling', 'academy_skilling.academySkillingId', '=', 'allocated_enlistees.academySkillingId')
                        ->join('gewog', function($join) use ($allocationId) {
                            $join->on('gewog.gewogId', '=', 'allocated_enlistees.gewogId')
                                ->where('allocated_enlistees.allocationId', '=', $allocationId);
                        })
                        ->join('dzongkhag', function ($join) use ($allocationId) {
                            $join->on('dzongkhag.dzongkhagId', '=', 'gewog.dzongkhagId')
                                ->where('allocated_enlistees.allocationId', '=', $allocationId);
                        })
                        ->where('allocation.allocationId', $setting->allocationId)
                        ->where('allocated_enlistees.academySkillingId', $setting->academySkillingId)
                        ->select('allocation.*', 'allocated_enlistees.*', 'gewog.*', 'dzongkhag.*')
                        ->get();
        
                    // Query to get the count of students grouped by dzongkhag for the specific academy
                    $dzongkhagStudents = DB::table('allocated_enlistees')
                        ->join('allocation', 'allocated_enlistees.allocationId', '=', 'allocation.allocationId')
                        ->join('academy_skilling', 'academy_skilling.academySkillingId', '=', 'allocated_enlistees.academySkillingId')
                        ->join('gewog', 'gewog.gewogId', '=', 'allocated_enlistees.gewogId')
                        ->join('dzongkhag', 'dzongkhag.dzongkhagId', '=', 'gewog.dzongkhagId')
                        ->where('allocation.allocationId', '=', $allocationId)
                        ->where('academy_skilling.academySkillingId', $setting->academySkillingId)
                        ->select('dzongkhag.dzongkhag as name', DB::raw('count(allocated_enlistees.CID) as TotalEnlistee'))
                        ->groupBy('dzongkhag.dzongkhag')
                        ->get();
        
                    // Query to get the count of male and female students for the specific academy
                    $genderCounts = DB::table('allocated_enlistees')
                        ->join('allocation', 'allocated_enlistees.allocationId', '=', 'allocation.allocationId')
                        ->join('academy_skilling', 'academy_skilling.academySkillingId', '=', 'allocated_enlistees.academySkillingId')
                        ->where('allocation.allocationId', '=', $allocationId)
                        ->where('academy_skilling.academySkillingId', $setting->academySkillingId)
                        ->select(
                            DB::raw('sum(case when allocated_enlistees.gender = "M" then 1 else 0 end) as maleCount'),
                            DB::raw('sum(case when allocated_enlistees.gender = "F" then 1 else 0 end) as femaleCount')
                        )
                        ->first();
        
                    // Transform the dzongkhag data into the required format, trimming dzongkhag name to the first 4 letters
                    $dzongkhagData = $dzongkhagStudents->map(function($item) {
                        return [
                            'name' => substr($item->name, 0, 4), // Trim the dzongkhag name to the first 4 letters
                            'TotalEnlistee' => $item->TotalEnlistee
                        ];
                    });
        
                    // Add the 'students' key, dzongkhag count data, and gender counts to each setting
                    $setting->students = $eachStudent;
                    $setting->dzongkhagCount = $dzongkhagData;
                    $setting->length = count($eachStudent);
                    $setting->maleCount = $genderCounts->maleCount;
                    $setting->femaleCount = $genderCounts->femaleCount;
        
                    $academyStudents[] = $setting;

                }

                $allSelectedAllocation[] = $academyStudents;
            }

            return response()->json([
                'message' => 'Allocation saved successfully!',
                'allocation' => $allSelectedAllocation,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function saveAllocation(int $id)
    {
        try {
            // Get the allocation by ID
            $allocation = Allocation::findOrFail($id);

            // Update the boolean field to true
            $allocation->selectedAllocation = true; // Replace 'yourBooleanField' with the actual field name

            // Save the changes
            $allocation->save();

            return response()->json([
                'message' => 'Allocation saved successfully!',
                'allocation' => $allocation,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function getAllCurrentAllocation(Request $req,int $id) {
        try {
            $allocations = Allocation::where('allocationId', $id)->first();
    
            return response()->json([
                'data' => $allocations,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getAcademyData(Request $req, int $id) {
        try {
            // Query to get all academies
            $academySetting = DB::table('academy')
                ->select('academy.*')
                ->get();
    
            $academyStudents = [];
    
            foreach ($academySetting as $setting) {
                // Query to get the count of students grouped by dzongkhag for the specific academy
                $dzongkhagStudents = DB::table('allocated_enlistees')
                    ->join('allocation', 'allocated_enlistees.allocationId', '=', 'allocation.allocationId')
                    ->join('academy_skilling', 'academy_skilling.academySkillingId', '=', 'allocated_enlistees.academySkillingId')
                    ->join('skilling', 'skilling.skillingId', '=', 'academy_skilling.skillingId')
                    ->join('gewog', 'gewog.gewogId', '=', 'allocated_enlistees.gewogId')
                    ->join('dzongkhag', 'dzongkhag.dzongkhagId', '=', 'gewog.dzongkhagId')
                    ->where('allocation.allocationId', '=', $id)
                    ->where('academy_skilling.academyId', $setting->academyId)
                    ->select('dzongkhag.dzongkhag as name', DB::raw('count(allocated_enlistees.CID) as TotalEnlistee'))
                    ->groupBy('dzongkhag.dzongkhag')
                    ->get();
    
                // Transform the data into the required format
                $data = $dzongkhagStudents->map(function($item) {
                    return [
                        'name' => substr($item->name, 0, 4),
                        'TotalEnlistee' => $item->TotalEnlistee
                    ];
                });

                $totalStudentCount = $data->sum('TotalEnlistee');
    
                $academyStudents[] = [
                    'academyId' => $setting->academyId,
                    'name' => $setting->name,
                    'capacity' => $setting->capacity,
                    'students' => $data,
                    'totalStudent' => $totalStudentCount
                ];
            }
    
            return response()->json($academyStudents);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
    
    public function getAllocation(Request $req, int $id) {
        try {
            // Query to get all academy skilling settings with distinct results
            $academySetting = DB::table('academy_skilling')
                ->join('skilling', 'skilling.skillingId', '=', 'academy_skilling.skillingId')
                ->join('setting_run', 'setting_run.academySkillingId', '=', 'academy_skilling.academySkillingId')
                ->join('setting', 'setting.settingId', '=', 'setting_run.settingId')
                ->join('allocation', 'allocation.allocationId', '=', 'setting_run.allocationId')
                ->where('allocation.allocationId', $id)
                ->select('academy_skilling.*', 'skilling.*', 'setting_run.*', 'setting.*', 'allocation.*')
                ->distinct()
                ->get();
    
            $academyStudents = [];
    
            foreach ($academySetting as $setting) {
                // Filter students for the specific academy
                $eachStudent = DB::table('allocated_enlistees')
                    ->join('allocation', 'allocated_enlistees.allocationId', '=', 'allocation.allocationId')
                    ->join('academy_skilling', 'academy_skilling.academySkillingId', '=', 'allocated_enlistees.academySkillingId')
                    ->join('gewog', function($join) use ($id) {
                        $join->on('gewog.gewogId', '=', 'allocated_enlistees.gewogId')
                            ->where('allocated_enlistees.allocationId', '=', $id);
                    })
                    ->join('dzongkhag', function ($join) use ($id) {
                        $join->on('dzongkhag.dzongkhagId', '=', 'gewog.dzongkhagId')
                            ->where('allocated_enlistees.allocationId', '=', $id);
                    })
                    ->where('allocation.allocationId', $setting->allocationId)
                    ->where('allocated_enlistees.academySkillingId', $setting->academySkillingId)
                    ->select('allocation.*', 'allocated_enlistees.*', 'gewog.*', 'dzongkhag.*')
                    ->get();
    
                // Query to get the count of students grouped by dzongkhag for the specific academy
                $dzongkhagStudents = DB::table('allocated_enlistees')
                    ->join('allocation', 'allocated_enlistees.allocationId', '=', 'allocation.allocationId')
                    ->join('academy_skilling', 'academy_skilling.academySkillingId', '=', 'allocated_enlistees.academySkillingId')
                    ->join('gewog', 'gewog.gewogId', '=', 'allocated_enlistees.gewogId')
                    ->join('dzongkhag', 'dzongkhag.dzongkhagId', '=', 'gewog.dzongkhagId')
                    ->where('allocation.allocationId', '=', $id)
                    ->where('academy_skilling.academySkillingId', $setting->academySkillingId)
                    ->select('dzongkhag.dzongkhag as name', DB::raw('count(allocated_enlistees.CID) as TotalEnlistee'))
                    ->groupBy('dzongkhag.dzongkhag')
                    ->get();
    
                // Query to get the count of male and female students for the specific academy
                $genderCounts = DB::table('allocated_enlistees')
                    ->join('allocation', 'allocated_enlistees.allocationId', '=', 'allocation.allocationId')
                    ->join('academy_skilling', 'academy_skilling.academySkillingId', '=', 'allocated_enlistees.academySkillingId')
                    ->where('allocation.allocationId', '=', $id)
                    ->where('academy_skilling.academySkillingId', $setting->academySkillingId)
                    ->select(
                        DB::raw('sum(case when allocated_enlistees.gender = "M" then 1 else 0 end) as maleCount'),
                        DB::raw('sum(case when allocated_enlistees.gender = "F" then 1 else 0 end) as femaleCount')
                    )
                    ->first();
    
                // Transform the dzongkhag data into the required format, trimming dzongkhag name to the first 4 letters
                $dzongkhagData = $dzongkhagStudents->map(function($item) {
                    return [
                        'name' => substr($item->name, 0, 4), // Trim the dzongkhag name to the first 4 letters
                        'TotalEnlistee' => $item->TotalEnlistee
                    ];
                });
    
                // Add the 'students' key, dzongkhag count data, and gender counts to each setting
                $setting->students = $eachStudent;
                $setting->dzongkhagCount = $dzongkhagData;
                $setting->length = count($eachStudent);
                $setting->maleCount = $genderCounts->maleCount;
                $setting->femaleCount = $genderCounts->femaleCount;
    
                $academyStudents[] = $setting;
            }
    
            return response()->json([
                'id' => $id,
                'academyStudents' => $academyStudents,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
    
    public function deleteAllocation(Request $req) {
        try {
            DB::beginTransaction();

            error_log("called");

            $allAllocation = $req->input('allocationIds');

            if(empty($allAllocation)) {
                throw new \Exception('No allocation IDs provided');
            }

            foreach($allAllocation as $id) {
                // Delete related AllocatedEnlistees records
                AllocatedEnlistees::where('allocationId', $id)->delete();
    
                // Get all SettingRun records related to the allocation
                $allsetting = SettingRun::where('allocationId', $id)->get();
    
                // Extract setting IDs
                $settingIds = $allsetting->pluck('settingId')->toArray();
    
                // Delete related SettingRun records
                SettingRun::where('allocationId', $id)->delete();
    
                // Delete related Setting records
                Setting::whereIn('settingId', $settingIds)->delete();
    
                // Finally, delete the Allocation record
                Allocation::where('allocationId', $id)->delete();
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => "deleted successfully!"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function getAllAllocation() {
        try {
            $allocations = Allocation::orderBy('created_at', 'desc')->get();

            return response()->json([
                'data' => $allocations,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function storeEnlistee($CID, $isDiversity, $academySkillingId, $allocationId, $intakeId) {
        DB::beginTransaction();
    
        try {

            $student = DB::table('enlistees')->where('CID', $CID)->first();
            error_log("student name ".$student->name);
            // Insert data into AllocatedEnlistees table
            $insertData = [
                'CID' => $CID,
                'name' => $student->name,
                'gender' => $student->gender,
                'gewogId' => $student->gewogId,
                'hasHigerEducation' => $student->hasHigerEducation,
                'math' => $student->math,
                'bmath' => $student->bmath,
                'english' => $student->english,
                'dzongkha' => $student->dzongkha,
                'weight' => $student->weight,
                'intakeId' => $intakeId,
                'allocationId' => $allocationId,
                'isDiversity' => $isDiversity,
                'academySkillingId' => $academySkillingId
            ];
    
            AllocatedEnlistees::insert($insertData);
    
            // Delete data from pre_allocation_temp table
            DB::table('pre_allocation_temp')->where('CID', $CID)->delete();
    
            DB::commit();
            // Log successful deletion
            error_log("Deleted record for CID: " . $CID . " from pre_allocation_temp.");
        } catch (\Exception $e) {
            DB::rollBack();
            // Log error if deletion fails
            error_log("Failed to delete record for CID: " . $CID . " from pre_allocation_temp. Error: " . $e->getMessage());
            throw $e; // Re-throw the exception to be caught and handled higher up in the call stack
        }
    }

    public function academicAllocation($gender,$skillingId,$academicCount,$academySkillingId,$allocationId,$intakeId,$preference) {
        if ($preference === 1) {
            $students = DB::select("
                SELECT *
                FROM pre_allocation_temp
                WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = '$gender'
                ORDER BY generalMath DESC LIMIT $academicCount
            ");

            foreach ($students as $student) {
                $CID = $student->CID;
                $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
            }

            return count($students);
        } else {
            $students = DB::select("
                SELECT *
                FROM pre_allocation_temp
                WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = '$gender'
                ORDER BY generalMath DESC LIMIT $academicCount
            ");

            foreach ($students as $student) {
                $CID = $student->CID;
                $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
            }

            return count($students);
        }
    }

    public function diversityIntake($students,$gender,$skillingId,$diversity,$academySkillingId,$allocationId,$intakeId,$preference) {
        error_log("diversity from function ".$diversity);
        error_log("intake id ".$intakeId);
        error_log("allocation id ".$allocationId);
        error_log("academy skilling id ".$academySkillingId);

        if ($preference === 1) {
            $sql = "
                SELECT dzongkhag, COUNT(*) AS count
                FROM pre_allocation_temp
                WHERE hasHigerEducation = true 
                    AND (preferenceOne = $skillingId OR noPreference = true) 
                    AND gender = '$gender'
                GROUP BY dzongkhag
                ORDER BY generalMath DESC;
            ";

            $dzongkhagCounts = DB::select($sql);

            $academyIndex = 0;

            $totalDzongkhagCount = 0;

            foreach ($dzongkhagCounts as $dzongkhag) {
                $totalDzongkhagCount += $dzongkhag->count;
            }

            error_log("dzongkhag count ".$totalDzongkhagCount);

            $totalDiversityAllocated = 0;

            for ($i = 0; $i < $diversity; $i++) {

                if ($totalDzongkhagCount === 0) {
                    break; // No more dzongkhags to consider
                }

                error_log("dzongkhag count ".$dzongkhagCounts[$academyIndex]->count);

                while ($dzongkhagCounts[$academyIndex]->count === 0) {
                    $academyIndex = ($academyIndex + 1) % count($dzongkhagCounts);
                }

                if ($dzongkhagCounts[$academyIndex]->count !== 0) {
                    error_log("academy index ".$academyIndex);   
                    error_log($dzongkhagCounts[$academyIndex]->dzongkhag);

                    $currentDzongkhag = $dzongkhagCounts[$academyIndex]->dzongkhag;

                    foreach ($students as $key => $student) {
                        if ($student->dzongkhag === $currentDzongkhag) {
                            error_log("got the student ".$student->CID);
                            $CID = $student->CID;
                            $this->storeEnlistee($CID,true,$academySkillingId,$allocationId,$intakeId);
                            unset($students[$key]);
                            $totalDzongkhagCount--;
                            $dzongkhagCounts[$academyIndex]->count--;
                            $totalDiversityAllocated++;
                            error_log("stored student ".$i);
                            break;
                        }
                    }

                    $academyIndex = ($academyIndex + 1) % count($dzongkhagCounts);
                }
            }

            return $totalDiversityAllocated;
        } else {
            $sql = "
                SELECT dzongkhag, COUNT(*) AS count
                FROM pre_allocation_temp
                WHERE hasHigerEducation = true 
                    AND preferenceTwo = $skillingId
                    AND gender = '$gender'
                GROUP BY dzongkhag
                ORDER BY generalMath DESC;
            ";

            $dzongkhagCounts = DB::select($sql);

            $academyIndex = 0;

            $totalDzongkhagCount = 0;

            foreach ($dzongkhagCounts as $dzongkhag) {
                $totalDzongkhagCount += $dzongkhag->count;
            }

            error_log("dzongkhag count ".$totalDzongkhagCount);

            $totalDiversityAllocated = 0;

            for ($i = 0; $i < $diversity; $i++) {

                if ($totalDzongkhagCount === 0) {
                    break; // No more dzongkhags to consider
                }

                error_log("dzongkhag count ".$dzongkhagCounts[$academyIndex]->count);

                while ($dzongkhagCounts[$academyIndex]->count === 0) {
                    $academyIndex = ($academyIndex + 1) % count($dzongkhagCounts);
                }

                if ($dzongkhagCounts[$academyIndex]->count !== 0) {
                    error_log("academy index ".$academyIndex);   
                    error_log($dzongkhagCounts[$academyIndex]->dzongkhag);

                    $currentDzongkhag = $dzongkhagCounts[$academyIndex]->dzongkhag;

                    foreach ($students as $key => $student) {
                        if ($student->dzongkhag === $currentDzongkhag) {
                            error_log("got the student ".$student->CID);
                            $CID = $student->CID;
                            $this->storeEnlistee($CID,true,$academySkillingId,$allocationId,$intakeId);
                            unset($students[$key]);
                            $totalDzongkhagCount--;
                            $dzongkhagCounts[$academyIndex]->count--;
                            $totalDiversityAllocated++;
                            error_log("stored student ".$i);
                            break;
                        }
                    }

                    $academyIndex = ($academyIndex + 1) % count($dzongkhagCounts);
                }
            }

            return $totalDiversityAllocated;
        }
    }

    public function createPreAllocationTable() {
        DB::beginTransaction();

        try {
            $sql = "CREATE OR REPLACE VIEW PreAllocation AS
                SELECT e.CID, e.name, e.gender, g.gewog, e.hasHigerEducation,
                    CASE
                        WHEN e.math IS NOT NULL THEN e.math * 1.1
                        WHEN e.bmath IS NOT NULL THEN e.bmath
                        ELSE NULL
                    END AS generalMath,
                    CASE
                        WHEN e.math IS NOT NULL THEN 'math'
                        WHEN e.bmath IS NOT NULL THEN 'bmath'
                        ELSE 'nomath'
                    END AS subject,
                    e.english, e.dzongkha, e.weight, e.preferenceOne,
                    e.preferenceTwo, e.noPreference, e.hasTenFinger, dz.dzongkhag
                FROM enlistees e
                JOIN gewog g ON e.gewogId = g.gewogId
                JOIN dzongkhag dz ON g.dzongkhagId = dz.dzongkhagId;";

            DB::statement($sql);

            DB::statement("DROP TABLE IF EXISTS pre_allocation_temp");

            DB::statement("CREATE TABLE pre_allocation_temp AS SELECT * FROM PreAllocation");

            error_log("executed");

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function fssAllocation(Request $request,$allocationId = null, $intakeId = null) {
        try {
            DB::beginTransaction();

            if ($request->has('allocationId')) {
                $allocationId = $request->input('allocationId');
                $intakeId = $request->input('intakeId');

                $academySkillingId = $request->input('academySkillingId');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;

                $students = DB::select("SELECT * FROM pre_allocation_temp LIMIT $intakeSize");

                foreach ($students as $student) {
                    $CID = $student->CID;
                    $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                }

                error_log("stored");

                $numberOfFemale = count(array_filter($students, function($student) {
                    return $student->gender === 'F';
                }));

                error_log("female ".$numberOfFemale);

                $numberOfMale = count($students) - $numberOfFemale;

                error_log("male ".$numberOfMale);

                // $femaleCount = floor($intakeSize / 2);

                // $studentFemale = DB::select("
                //     SELECT *
                //     FROM pre_allocation_temp
                //     WHERE gender = 'F'
                //     LIMIT $femaleCount
                // ");

                // foreach ($studentFemale as $student) {
                //     $CID = $student->CID;
                //     $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                // }

                // $maleCount = $intakeSize - $femaleCount;

                // $studentMale = DB::select("
                //     SELECT *
                //     FROM pre_allocation_temp
                //     WHERE gender = 'M'
                //     LIMIT $maleCount
                // ");

                // foreach ($studentMale as $student) {
                //     $CID = $student->CID;
                //     $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                // }
    
                $setting = Setting::create([
                    'diversityIntake' => 0,
                    'numberOfFemale' => $numberOfFemale,
                    'numberOfMale' => $numberOfMale
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);

                DB::commit();

                return response()->json([
                    "status" => 'success',
                    "message" => "Allocation Completed!"
                ],200);
            } else {
                error_log("no allocation id");
                error_log("allocation id from hss ".$allocationId);
    
                $academySkillingId = $request->input('academySkillingId');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;

                $femaleCount = floor($intakeSize / 2);

                $studentFemale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE gender = 'F'
                    LIMIT $femaleCount
                ");

                foreach ($studentFemale as $student) {
                    $CID = $student->CID;
                    $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                }

                $maleCount = $intakeSize - $femaleCount;

                $studentMale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE gender = 'M'
                    LIMIT $maleCount
                ");

                foreach ($studentMale as $student) {
                    $CID = $student->CID;
                    $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                }
    
                $setting = Setting::create([
                    'diversityIntake' => 0,
                    'numberOfFemale' => count($studentFemale),
                    'numberOfMale' => count($studentMale)
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);
    
                return count($studentFemale) + count($studentMale);
            }

            DB::commit();
    
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function hssAllocation(Request $request,$allocationId = null, $intakeId = null) {
        try {
            DB::beginTransaction();

            if ($request->has('allocationId')) {
                $allocationId = $request->input('allocationId');
                $intakeId = $request->input('intakeId');

                $academySkillingId = $request->input('academySkillingId');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;

                $femaleCount = floor($intakeSize / 2);

                $studentFemale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE gender = 'F' AND hasTenFinger = 1
                    LIMIT $femaleCount
                ");

                foreach ($studentFemale as $student) {
                    $CID = $student->CID;
                    $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                }

                $maleCount = $intakeSize - $femaleCount;

                $studentMale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE gender = 'M' AND hasTenFinger = 1
                    LIMIT $maleCount
                ");

                foreach ($studentMale as $student) {
                    $CID = $student->CID;
                    $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                }
    
                $setting = Setting::create([
                    'diversityIntake' => 0,
                    'numberOfFemale' => count($studentFemale),
                    'numberOfMale' => count($studentMale)
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);

                DB::commit();

                return response()->json([
                    "status" => 'success',
                    "message" => "Allocation Completed!"
                ],200);
            } else {
                error_log("no allocation id");
                error_log("allocation id from hss ".$allocationId);
    
                $academySkillingId = $request->input('academySkillingId');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;

                $femaleCount = floor($intakeSize / 2);

                $studentFemale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE gender = 'F' AND hasTenFinger = 1
                    LIMIT $femaleCount
                ");

                foreach ($studentFemale as $student) {
                    $CID = $student->CID;
                    $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                }

                $maleCount = $intakeSize - $femaleCount;

                $studentMale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE gender = 'M' AND hasTenFinger = 1
                    LIMIT $maleCount
                ");

                foreach ($studentMale as $student) {
                    $CID = $student->CID;
                    $this->storeEnlistee($CID, false, $academySkillingId, $allocationId, $intakeId);
                }
    
                $setting = Setting::create([
                    'diversityIntake' => 0,
                    'numberOfFemale' => count($studentFemale),
                    'numberOfMale' => count($studentMale)
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);
    
                return count($studentFemale) + count($studentMale);
            }

            DB::commit();
    
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function cssAllocation(Request $request,$allocationId = null, $intakeId = null) {
        try {
            DB::beginTransaction();

            if ($request->has('allocationId')) {
                $allocationId = $request->input('allocationId');
                $intakeId = $request->input('intakeId');

                $academySkillingId = $request->input('academySkillingId');
                $configuration = $request->input('configuration');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;
    
                $femaleCount = $configuration['femaleCount'];
                $maleCount = $intakeSize - $femaleCount;
    
                $academyMark = $configuration['academyMark'];
                $diversity = 100 - $academyMark;
    
                error_log("capacity ".$academySkilling->intakeSize);
                error_log("female ".$femaleCount);
                error_log("male ".$maleCount);
                error_log("academy mark ".$academyMark);
                error_log("diversity from ict ".$diversity);
    
                $setting = Setting::create([
                    'diversityIntake' => $diversity,
                    'numberOfFemale' => $femaleCount,
                    'numberOfMale' => $maleCount
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);
    
                $studentsWithHigherEducationFemale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'F'
                    ORDER BY english DESC
                ");
    
                $femaleDiversity;
                $maleDiversity;
                $femaleAcademic;
                $maleAcademic;
    
                $actualDiversityIntakeFemale = ($diversity / 100) * $femaleCount;
                $actualAcademicIntakeFemale = ($academyMark / 100) * $femaleCount;
    
                $actualDiversityIntakeMale = ($diversity / 100) * $maleCount;
                $actualAcademicIntakeMale = ($academyMark / 100) * $maleCount;
    
                if ($actualDiversityIntakeFemale != floor($actualDiversityIntakeFemale)) {
                    $femaleDiversity = floor($actualDiversityIntakeFemale) + 1;
                    $femaleAcademic = floor($actualAcademicIntakeFemale);
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = floor($actualDiversityIntakeMale);
                    $maleAcademic = floor($actualAcademicIntakeMale) + 1;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                } else {
                    error_log("error");
    
                    $femaleDiversity = $actualDiversityIntakeFemale;
                    $femaleAcademic = $actualAcademicIntakeFemale;
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = $actualDiversityIntakeMale;
                    $maleAcademic = $actualAcademicIntakeMale;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                }
    
                $totalDiversityAllocated = $this->diversityIntake($studentsWithHigherEducationFemale,'F',$skillingId,$femaleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
    
                //if insufficient student from preference one
    
                if (intval($totalDiversityAllocated) !== intval($femaleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationFemaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'F'
                        ORDER BY english DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationFemaleFromTwo,'F',$skillingId,intval($femaleDiversity) - intval($totalDiversityAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //academy mark allocation female
    
                $femaleAcademicAllocated = $this->academicAllocation('F',$skillingId,$femaleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($femaleAcademicAllocated) !== intval($femaleAcademic)) {
                    $this->academicAllocation('F',$skillingId,intval($femaleAcademic) - intval($femaleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
    
                //male diversity
    
    
                $studentsWithHigherEducationMale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'M'
                    ORDER BY english DESC
                ");
    
                $totalDiversityAllocatedMale = $this->diversityIntake($studentsWithHigherEducationMale,'M',$skillingId,$maleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($totalDiversityAllocatedMale) !== intval($maleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationMaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'M'
                        ORDER BY english DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationMaleFromTwo,'M',$skillingId,intval($maleDiversity) - intval($totalDiversityAllocatedMale),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //acadmic allocation for male
    
                $maleAcademicAllocated = $this->academicAllocation('M',$skillingId,$maleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($maleAcademicAllocated) !== intval($maleAcademic)) {
                    $this->academicAllocation('M',$skillingId,intval($maleAcademic) - intval($maleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }

                DB::commit();

                return response()->json([
                    "status" => 'success',
                    "message" => "Allocation Completed!"
                ],200);
            } else {
                error_log("no allocation id");
                error_log("allocation id from css ".$allocationId);
                $academySkillingId = $request->input('academySkillingId');
                $configuration = $request->input('configuration');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;
    
                $femaleCount = $configuration['femaleCount'];
                $maleCount = $intakeSize - $femaleCount;
    
                $academyMark = $configuration['academyMark'];
                $diversity = 100 - $academyMark;
    
                error_log("capacity ".$academySkilling->intakeSize);
                error_log("female ".$femaleCount);
                error_log("male ".$maleCount);
                error_log("academy mark ".$academyMark);
                error_log("diversity from ict ".$diversity);
    
                $setting = Setting::create([
                    'diversityIntake' => $diversity,
                    'numberOfFemale' => $femaleCount,
                    'numberOfMale' => $maleCount
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);
    
                $studentsWithHigherEducationFemale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'F'
                    ORDER BY english DESC
                ");
    
                $femaleDiversity;
                $maleDiversity;
                $femaleAcademic;
                $maleAcademic;
    
                $actualDiversityIntakeFemale = ($diversity / 100) * $femaleCount;
                $actualAcademicIntakeFemale = ($academyMark / 100) * $femaleCount;
    
                $actualDiversityIntakeMale = ($diversity / 100) * $maleCount;
                $actualAcademicIntakeMale = ($academyMark / 100) * $maleCount;
    
                if ($actualDiversityIntakeFemale != floor($actualDiversityIntakeFemale)) {
                    $femaleDiversity = floor($actualDiversityIntakeFemale) + 1;
                    $femaleAcademic = floor($actualAcademicIntakeFemale);
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = floor($actualDiversityIntakeMale);
                    $maleAcademic = floor($actualAcademicIntakeMale) + 1;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                } else {
                    error_log("error");
    
                    $femaleDiversity = $actualDiversityIntakeFemale;
                    $femaleAcademic = $actualAcademicIntakeFemale;
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = $actualDiversityIntakeMale;
                    $maleAcademic = $actualAcademicIntakeMale;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                }
    
                $totalDiversityAllocated = $this->diversityIntake($studentsWithHigherEducationFemale,'F',$skillingId,$femaleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
    
                //if insufficient student from preference one
    
                if (intval($totalDiversityAllocated) !== intval($femaleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationFemaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'F'
                        ORDER BY english DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationFemaleFromTwo,'F',$skillingId,intval($femaleDiversity) - intval($totalDiversityAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //academy mark allocation female
    
                $femaleAcademicAllocated = $this->academicAllocation('F',$skillingId,$femaleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($femaleAcademicAllocated) !== intval($femaleAcademic)) {
                    $this->academicAllocation('F',$skillingId,intval($femaleAcademic) - intval($femaleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
    
                //male diversity
    
    
                $studentsWithHigherEducationMale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'M'
                    ORDER BY english DESC
                ");
    
                $totalDiversityAllocatedMale = $this->diversityIntake($studentsWithHigherEducationMale,'M',$skillingId,$maleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($totalDiversityAllocatedMale) !== intval($maleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationMaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'M'
                        ORDER BY english DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationMaleFromTwo,'M',$skillingId,intval($maleDiversity) - intval($totalDiversityAllocatedMale),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //acadmic allocation for male
    
                $maleAcademicAllocated = $this->academicAllocation('M',$skillingId,$maleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($maleAcademicAllocated) !== intval($maleAcademic)) {
                    $this->academicAllocation('M',$skillingId,intval($maleAcademic) - intval($maleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                return $totalDiversityAllocated;
            }

            DB::commit();
    
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function ictAllocation(Request $request,$allocationId = null,$intakeId = null) {

        try {
            DB::beginTransaction();

            if ($request->has('allocationId')) {
                $allocationId = $request->input('allocationId');
                $intakeId = $request->input('intakeId');

                $academySkillingId = $request->input('academySkillingId');
                $configuration = $request->input('configuration');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;
    
                $femaleCount = $configuration['femaleCount'];
                $maleCount = $intakeSize - $femaleCount;
    
                $academyMark = $configuration['academyMark'];
                $diversity = 100 - $academyMark;
    
                error_log("capacity ".$academySkilling->intakeSize);
                error_log("female ".$femaleCount);
                error_log("male ".$maleCount);
                error_log("academy mark ".$academyMark);
                error_log("diversity from ict ".$diversity);
    
                $setting = Setting::create([
                    'diversityIntake' => $diversity,
                    'numberOfFemale' => $femaleCount,
                    'numberOfMale' => $maleCount
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);
    
                $studentsWithHigherEducationFemale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'F'
                    ORDER BY generalMath DESC
                ");
    
                $femaleDiversity;
                $maleDiversity;
                $femaleAcademic;
                $maleAcademic;
    
                $actualDiversityIntakeFemale = ($diversity / 100) * $femaleCount;
                $actualAcademicIntakeFemale = ($academyMark / 100) * $femaleCount;
    
                $actualDiversityIntakeMale = ($diversity / 100) * $maleCount;
                $actualAcademicIntakeMale = ($academyMark / 100) * $maleCount;
    
                if ($actualDiversityIntakeFemale != floor($actualDiversityIntakeFemale)) {
                    $femaleDiversity = floor($actualDiversityIntakeFemale) + 1;
                    $femaleAcademic = floor($actualAcademicIntakeFemale);
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = floor($actualDiversityIntakeMale);
                    $maleAcademic = floor($actualAcademicIntakeMale) + 1;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                } else {
                    error_log("error");
    
                    $femaleDiversity = $actualDiversityIntakeFemale;
                    $femaleAcademic = $actualAcademicIntakeFemale;
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = $actualDiversityIntakeMale;
                    $maleAcademic = $actualAcademicIntakeMale;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                }
    
                $totalDiversityAllocated = $this->diversityIntake($studentsWithHigherEducationFemale,'F',$skillingId,$femaleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
    
                //if insufficient student from preference one
    
                if (intval($totalDiversityAllocated) !== intval($femaleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationFemaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'F'
                        ORDER BY generalMath DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationFemaleFromTwo,'F',$skillingId,intval($femaleDiversity) - intval($totalDiversityAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //academy mark allocation female
    
                $femaleAcademicAllocated = $this->academicAllocation('F',$skillingId,$femaleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($femaleAcademicAllocated) !== intval($femaleAcademic)) {
                    $this->academicAllocation('F',$skillingId,intval($femaleAcademic) - intval($femaleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
    
                //male diversity
    
    
                $studentsWithHigherEducationMale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'M'
                    ORDER BY generalMath DESC
                ");
    
                $totalDiversityAllocatedMale = $this->diversityIntake($studentsWithHigherEducationMale,'M',$skillingId,$maleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($totalDiversityAllocatedMale) !== intval($maleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationMaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'M'
                        ORDER BY generalMath DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationMaleFromTwo,'M',$skillingId,intval($maleDiversity) - intval($totalDiversityAllocatedMale),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //acadmic allocation for male
    
                $maleAcademicAllocated = $this->academicAllocation('M',$skillingId,$maleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($maleAcademicAllocated) !== intval($maleAcademic)) {
                    $this->academicAllocation('M',$skillingId,intval($maleAcademic) - intval($maleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }

                DB::commit();

                return response()->json([
                    "status" => 'success',
                    "message" => "Allocation Completed!"
                ],200);

            } else {
                error_log("starting ict");
                error_log("no allocation id");
                error_log("allocation id from ict ".$allocationId);
                $academySkillingId = $request->input('academySkillingId');
                $configuration = $request->input('configuration');
    
                $academySkilling =  AcademySkilling::find($academySkillingId);
                $intakeSize = $academySkilling->intakeSize;
                $skillingId = $academySkilling->skillingId;
    
                $femaleCount = $configuration['femaleCount'];
                $maleCount = $intakeSize - $femaleCount;
    
                $academyMark = $configuration['academyMark'];
                $diversity = 100 - $academyMark;
    
                error_log("capacity ".$academySkilling->intakeSize);
                error_log("female ".$femaleCount);
                error_log("male ".$maleCount);
                error_log("academy mark ".$academyMark);
                error_log("diversity from ict ".$diversity);
    
                $setting = Setting::create([
                    'diversityIntake' => $diversity,
                    'numberOfFemale' => $femaleCount,
                    'numberOfMale' => $maleCount
                ]);
    
                $settingId = $setting->settingId;
    
                error_log("settingid ".$settingId);
    
                $settingRun = SettingRun::create([
                    'allocationId' => $allocationId,
                    'skillingId' => $skillingId,
                    'settingId' => $settingId,
                    'academySkillingId' => $academySkillingId
                ]);
    
                $studentsWithHigherEducationFemale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'F'
                    ORDER BY generalMath DESC
                ");
    
                $femaleDiversity;
                $maleDiversity;
                $femaleAcademic;
                $maleAcademic;
    
                $actualDiversityIntakeFemale = ($diversity / 100) * $femaleCount;
                $actualAcademicIntakeFemale = ($academyMark / 100) * $femaleCount;
    
                $actualDiversityIntakeMale = ($diversity / 100) * $maleCount;
                $actualAcademicIntakeMale = ($academyMark / 100) * $maleCount;
    
                if ($actualDiversityIntakeFemale != floor($actualDiversityIntakeFemale)) {
                    $femaleDiversity = floor($actualDiversityIntakeFemale) + 1;
                    $femaleAcademic = floor($actualAcademicIntakeFemale);
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = floor($actualDiversityIntakeMale);
                    $maleAcademic = floor($actualAcademicIntakeMale) + 1;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                } else {
                    error_log("error");
    
                    $femaleDiversity = $actualDiversityIntakeFemale;
                    $femaleAcademic = $actualAcademicIntakeFemale;
    
                    error_log("female diversity ".$femaleDiversity);
                    error_log("female academic ".$femaleAcademic);
    
                    $maleDiversity = $actualDiversityIntakeMale;
                    $maleAcademic = $actualAcademicIntakeMale;
    
                    error_log("male diversity ".$maleDiversity);
                    error_log("male academic ".$maleAcademic);
                }
    
                $totalDiversityAllocated = $this->diversityIntake($studentsWithHigherEducationFemale,'F',$skillingId,$femaleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
    
                //if insufficient student from preference one
    
                if (intval($totalDiversityAllocated) !== intval($femaleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationFemaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'F'
                        ORDER BY generalMath DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationFemaleFromTwo,'F',$skillingId,intval($femaleDiversity) - intval($totalDiversityAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //academy mark allocation female
    
                $femaleAcademicAllocated = $this->academicAllocation('F',$skillingId,$femaleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($femaleAcademicAllocated) !== intval($femaleAcademic)) {
                    $this->academicAllocation('F',$skillingId,intval($femaleAcademic) - intval($femaleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
    
                //male diversity
    
    
                $studentsWithHigherEducationMale = DB::select("
                    SELECT *
                    FROM pre_allocation_temp
                    WHERE hasHigerEducation = true AND (preferenceOne = $skillingId OR noPreference = true) AND gender = 'M'
                    ORDER BY generalMath DESC
                ");
    
                $totalDiversityAllocatedMale = $this->diversityIntake($studentsWithHigherEducationMale,'M',$skillingId,$maleDiversity,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($totalDiversityAllocatedMale) !== intval($maleDiversity)) {
                    error_log("allocated again");
                    $studentsWithHigherEducationMaleFromTwo = DB::select("
                        SELECT *
                        FROM pre_allocation_temp
                        WHERE hasHigerEducation = true AND preferenceTwo = $skillingId AND gender = 'M'
                        ORDER BY generalMath DESC
                    ");
    
                    $this->diversityIntake($studentsWithHigherEducationMaleFromTwo,'M',$skillingId,intval($maleDiversity) - intval($totalDiversityAllocatedMale),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                //acadmic allocation for male
    
                $maleAcademicAllocated = $this->academicAllocation('M',$skillingId,$maleAcademic,$academySkillingId,$allocationId,$intakeId,1);
    
                if (intval($maleAcademicAllocated) !== intval($maleAcademic)) {
                    $this->academicAllocation('M',$skillingId,intval($maleAcademic) - intval($maleAcademicAllocated),$academySkillingId,$allocationId,$intakeId,2);
                }
    
                return $totalDiversityAllocated;
            }

            DB::commit();
    
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function startAllocation(Request $request) {

        DB::beginTransaction();

        $name = $request->input('name');
        $year = $request->input('year');
        $configuration = $request->input('configuration');
        $academySkillingId = $request->input('academySkillingId');

        error_log("academy skilling id ".$academySkillingId);

        try {
            $intake = Intake::firstOrCreate(['year' => $year]);

            $intakeId = $intake->intakeId;

            error_log("intake id ".$intakeId);
            error_log("name ".$name);
            error_log("year ".$year);
            error_log("cofiguration ".$configuration['skilling']);

            $user = Auth::user();

            $allocation = Allocation::create([
                'intakeId' => $intakeId,
                'nameOfAllocation' => $name,
                'selectedAllocation' => false,
                'userId' => $user->userId
            ]);

            $allocationId = $allocation->allocationId;

            error_log("allocaiton id ".$allocationId);

            $this->createPreAllocationTable();

            $skillingName = $configuration['skilling'];

            if ($skillingName === 'ICT') {
                $a = $this->ictAllocation($request,$allocationId,$intakeId);
            } elseif ($skillingName === 'CSS') {
                $a = $this->cssAllocation($request,$allocationId,$intakeId);
            } elseif ($skillingName === 'HSS') {
                $a = $this->hssAllocation($request,$allocationId,$intakeId);
            } elseif ($skillingName === 'FSS') {
                $a = $this->fssAllocation($request,$allocationId,$intakeId);
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => ['allocationId' => $allocationId,'intakeId' => $intakeId]
            ]);

        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],500);
        }
    }
}
