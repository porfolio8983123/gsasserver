<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AcademySkilling;
use App\Models\Academy;

class AcademySkillingController extends Controller
{
    public function getAcademySkilling(Request $request,int $id) {

        $academySkilling = AcademySkilling::find($id);

        return response()->json([
            'data' => $academySkilling
        ]);
    }

    public function getAcademy(Request $request, int $id) {
        $academy = Academy::find($id);
        return response()->json([
            'data' => $academy
        ]);
    }
}
