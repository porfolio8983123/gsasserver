<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Skilling;
use App\Models\AcademySkilling;
use App\Models\Academy;

class SkillingController extends Controller
{
    public function getSkilling() {
        $skillings = Skilling::all();
        return response()->json([
            'data'=>$skillings
        ]);
    }

    public function editAcademySkilling(Request $request,$id) {
        try {
            $request->validate([
                'skilling'=>'required',
                'intakeSize'=>'required',
            ]);

            $skilling = Skilling::where('skillingName',$request->skilling)->get();

            $academySkilling = AcademySkilling::find($id);

            $academyId = AcademySkilling::find($id)->academyId;

            $totalSkillingCapacity = AcademySkilling::where('academyId', $academyId)
                ->where('academySkillingId', '!=', $id)
                ->sum('intakeSize');

            $totalNewIntakeSize = $request->intakeSize + $totalSkillingCapacity;

            $academyCapacity = Academy::find($academyId)->capacity;

            if (count($skilling) <= 0) {
                return response()->json([
                    "error"=> "Skilling doesn't exist!"
                ],404);
            }

            if ($totalNewIntakeSize <= $academyCapacity) {
                $academySkilling->update([
                    'skillingId'=>$skilling[0]->skillingId,
                    'intakeSize'=>$request->intakeSize
                ]);

                return response()->json([
                    'message'=>"Updated successfully!"
                ]);
            } else {
                return response()->json([
                    'error'=>"Sum of total intake is exceeding the capacity of an academy!"
                ],404);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error'=>$e->getMessage()
            ],500);
        }
    }

    public function addNewService(Request $request) {
        try {

            $request->validate([
                'academyId'=>'required',
                'skilling' => 'required',
                'intakeSize' => 'required'
            ]);

            $skilling = Skilling::where('skillingName',$request->skilling)->get();

            $existingSkilling = AcademySkilling::where('academyId', $request->academyId)
                ->where('skillingId', $skilling[0]->skillingId)
                ->exists();

            if ($existingSkilling) {
                return response()->json([
                    "error" => "Skilling already exists for the academy!"
                ], 400); // Bad Request
            }

            $totalServiceCapacity = AcademySkilling::where('academyId',$request->academyId)->sum('intakeSize');

            $academyCapacity = Academy::find($request->academyId)->capacity;

            $totalCapacity = $totalServiceCapacity + $request->intakeSize;

            $skilling = Skilling::where('skillingName',$request->skilling)->get();

            if (count($skilling) <= 0) {
                return response()->json([
                    "error"=> "Skilling doesn't exist!"
                ],404);
            }

            if ($totalCapacity <= $academyCapacity) {

                AcademySkilling::create([
                    "academyId" => $request->academyId,
                    "skillingId"=> $skilling[0]->skillingId,
                    "intakeSize"=> $request->intakeSize
                ]);

                return response()->json([
                    "message"=>"Skilling added successfully!"
                ],200);
            } else {
                return response()->json([
                    "error"=> "Sum of total intake is exceeding the capacity of an academy!"
                ],404);
            }  
        } catch (\Exception $e) {
            return response()->json([
                'error'=>$e->getMessage()
            ]);
        }
    }

    public function deleteService(Request $request, $id) {
        try {
            $academySkilling = AcademySkilling::findOrFail($id);
            $academySkilling->delete();

            return response()->json([
                'message' => "Delete successfully!"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error'=>$e->getMessage()
            ]);
        }
    }
}
