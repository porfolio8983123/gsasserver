<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dzongkhag;
use App\Models\Academy;
use DB;

class AcademyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $allAcademies = [];

        $academies = DB::table('academy')
            ->join('dzongkhag','academy.dzongkhagId','=','dzongkhag.dzongkhagId')
            ->select('academy.*','dzongkhag.dzongkhag')
            ->get();

        foreach($academies as $academy) {

            $academyId = $academy->academyId;
            
            $skills = DB::table('academy_skilling')
                ->join('skilling', 'academy_skilling.skillingId', '=', 'skilling.skillingId')
                ->where('academy_skilling.academyId', $academyId)
                ->select('skilling.*','academy_skilling.*')
                ->get();
            $academy->skills = $skills;
            $allAcademies[] = $academy;
        }

        return response()->json([
            "academies"=>$allAcademies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'capacity'=>'required',
            'dzongkhag'=>'required',
            'file'=>'required',
        ]);

        try {
            $result = Dzongkhag::select('dzongkhagId')
                ->where('dzongkhag', $request->dzongkhag)
                ->get();

            $academyName = Academy::where('name', $request->name)->get();

            if (count($academyName) > 0) {
                return response()->json(['error' => 'Such Academy Name Already Exist!'], 404);
            }

            if (count($result) > 0 && $request->has('file')) {
                $dzongkhagId = $result[0]->dzongkhagId;

                $fileName = $request->input('name');
                $file = $request->file('file');

                $file->move('academyImages',$fileName);

                Academy::create([
                    "name" => $request->name,
                    "capacity" => $request->capacity,
                    "dzongkhagId" => $dzongkhagId,
                    "status" => true,
                    "imagePath"=>$fileName
                ]);
                return response()->json(['message' => 'Academy created successfully'], 200);
            } else {
                return response()->json([
                    "error"=>"Dzongkhag Not Found"
                ],404);
            }
        } catch (\Exception $e) {
            return response()->json([
                "error"=>$e->getMessage()
            ],500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {

            $request->validate([
                'name'=>'required',
                'capacity'=>'required',
                'dzongkhag'=>'required'
            ]);

            $academy = Academy::findOrFail($id);

            $result = DB::select('SELECT dzongkhagId FROM dzongkhag WHERE dzongkhag = ?', [$request->dzongkhag]);

            if (count($result) > 0) {
                $dzongkhagId = $result[0]->dzongkhagId;

                $academy->update([
                    'name'=>$request->name,
                    'capacity'=>$request->capacity,
                    'dzongkhagId'=>$dzongkhagId
                ]);
                return response()->json(['message' => 'Academy updated successfully'], 200);
            } else {
                return response()->json([
                    "error"=>"Dzongkhag Not Found"
                ],404);
            }


        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function updateStatus(Request $request,string $id) {
        try {
            error_log("academy id ".$id);
            error_log("status ".$request->status);
            $academy = Academy::findOrFail($id);

            $request->validate([
                "status"=>'required|boolean'
            ]);

            $academy->update([
                'status'=>$request->status
            ]);

            return response()->json([
                'message'=>'Academy Status Updated Successfully!'
            ],200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update academy status'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
