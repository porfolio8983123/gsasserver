<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dzongkhag;

class DzongkhagController extends Controller
{
    public function index() {
        try {
            $dzongkhags = Dzongkhag::all();
    
            return response()->json([
                'status' => 'success',
                'data' => $dzongkhags,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
