<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class AuthenticationController extends Controller
{

    public function register(Request $req) {
        $req->validate([
            'name'=>'required|max:255',
            'email'=>'required|unique:users|max:255',
            'password'=>'required|min:8'
        ]);

        $user = User::create([
            'name'=>$req->name,
            'email'=>$req->email,
            'password'=>Hash::make($req->password)
        ]);

        $token = $user->createToken('auth_token')->accessToken;

        return response([
            'token'=>$token
        ]);
    }

    public function login(Request $req) {

        $req->validate([
            'email'=>'required',
            'password'=>'required'
        ]);

        error_log("logging in");

        try {
            $user = User::where('email',$req->email)->first();

            if (!$user || !Hash::check($req->password,$user->password)) {
                return response([
                    'message' => 'The Credentails are Incorrect!',
                ],401);
            }

            $token = $user->createToken('auth_token')->accessToken;

            $user->touch();

            return response([
                'token'=>$token
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'error'=>$e->getMessage()
            ]);
        }
    }

    public function logout(Request $req) {
        $req->user()->token()->revoke();

        return response([
            'message' => 'Logged Out Successfully!'
        ]);
    }
}
