<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllocatedEnlistees extends Model
{
    use HasFactory;

    protected $table = "allocated_enlistees";
    protected $primaryKey = 'CID';
    protected $fillable = ['CID','intakeId','allocationId','isDiversity','name','gender','gewogId','hasHigerEducation','math','bmath','english','dzongkha','weight'];
}
