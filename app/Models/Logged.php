<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logged extends Model
{
    use HasFactory;

    protected $table = "logged";
    protected $fillable = ['userId','action'];
}
