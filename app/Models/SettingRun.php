<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingRun extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = "setting_run";
    protected $primaryKey = "settingRunId";
    protected $fillable = ['allocationId','skillingId','settingId','academySkillingId'];
}
