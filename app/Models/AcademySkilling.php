<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademySkilling extends Model
{
    use HasFactory;

    protected $primaryKey = "academySkillingId";

    protected $table = "academy_skilling";
    protected $fillable = ['academyId','skillingId','intakeSize'];
}
