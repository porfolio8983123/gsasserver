<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dzongkhag;

class Academy extends Model
{
    use HasFactory;

    protected $primaryKey = "academyId";

    public $timestamps = false;

    protected $table = "academy";
    protected $fillable = ['name','capacity','dzongkhagId','status','imagePath'];

    public function dzongkhag()
    {
        return $this->belongsTo(Dzongkhag::class, 'dzongkhagId');
    }
}
