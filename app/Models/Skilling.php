<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skilling extends Model
{
    use HasFactory;

    protected $primaryKey = "skillingId";

    protected $table = "skilling";
    protected $fillable = ['skillingName','abbrebation'];
}
