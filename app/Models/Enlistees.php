<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enlistees extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = "enlistees";
    protected $primaryKey = 'CID';
    protected $fillable = ['CID','name','gender','gewogId','hasHigerEducation','math','bmath','english','dzongkha','weight','preferenceOne','preferenceTwo','noPreference','hasTenFinger'];
}
