<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Acadmey;

class Dzongkhag extends Model
{
    use HasFactory;

    protected $table = 'dzongkhag';
    protected $fillable = ['dzongkhag'];

    public function academies()
    {
        return $this->hasMany(Academy::class, 'dzongkhagId');
    }
}
